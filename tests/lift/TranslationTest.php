<?php
/**
 * UNIT test for LIFT Translation units
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\LIFT\Translation;

class TranslationTest extends TestCase
{
	private $dom;

	/* ====================================================================== */
	
	public function setUp()
	{
		$this->dom = new \DOMDocument("1.0", "UTF-8");
		$this->dom->formatOutput = true;
	}

	/* ====================================================================== */

	public function testCanGetId()
	{
		$translation = new Translation('foo', 'pl', 'fixed-id');

		$this->assertEquals('fixed-id', $translation->getId());
	}

	/* ====================================================================== */
	
	public function testCanGetEntry()
	{
		$t1 = new Translation('foo', 'pl', 'fixed-id');
		$t1->addDefinition('foo-definition1');
		$t1->addDefinition('foo-definition2');
		$t1->addExample('foo-example', 'de');

		$entry1 = $t1->getEntry($this->dom);
		$this->dom->appendChild($entry1);
		$xml = $this->dom->saveXML();

		$this->assertContains('<sense id="fixed-id">', $xml);
		$this->assertContains('<text>foo</text>', $xml);
		
		$this->assertContains('<definition>', $xml);
		$this->assertContains('foo-definition1', $xml);
		$this->assertContains('foo-definition2', $xml);

		$this->assertContains('<example>', $xml);
		$this->assertContains('foo-example', $xml);
	}

	/* ====================================================================== */
	
	public function testCanGetEntryWithSpecialChars()
	{
		$t2 = new Translation('bar & bar', 'pl');
		$t2->addNote('bar-note <&>');

		$entry2 = $t2->getEntry($this->dom);
		$this->dom->appendChild($entry2);
		$xml = $this->dom->saveXML();

		$this->assertContains('<sense id=', $xml);

		$this->assertContains('<text>bar &amp; bar</text>', $xml);
		$this->assertContains('<text>bar-note &lt;&amp;&gt;</text>', $xml);
	}
}