<?php
/**
 * UNIT test for LIFT dictinary creation
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\LIFT\Dictionary;
use ArteQ\LIFT\Term;
use ArteQ\LIFT\Translation;

class DictionaryTest extends TestCase
{
	private $dict;

	/* ====================================================================== */
	
	public function setUp()
	{
		$dict = new Dictionary();

		$this->dict = $dict;
	}

	/* ====================================================================== */
	
	public function testCanGenerate()
	{
		$xml = $this->dict->generate();

		$this->assertTrue(is_string($xml));
		$this->assertContains('<lift', $xml);
	}

	/* ====================================================================== */
	
	public function testCanSaveToFile()
	{
		$path = __DIR__.'/../tmp/dict.lift';
		@unlink($path);

		$file = $this->dict->save($path);
		$this->assertNotFalse($file);
		$this->assertTrue(is_file($path));

		@unlink($path);
	}

	/* ====================================================================== */
	
	public function testCanAddTerm()
	{
		$term = new Term('word', 'en');
		$this->dict->addTerm($term);

		$term = new Term('another term', 'en', '123-123-123');
		$this->dict->addTerm($term);

		$xml = $this->dict->generate();
		$this->assertContains('<lexical-unit', $xml);
		$this->assertContains('<entry', $xml);
		$this->assertContains('word', $xml);
	}

	/* ====================================================================== */
	
	public function testCanAddTranslation()
	{
		$term = new Term('word', 'en');

		$translation = new Translation('słowo', 'pl');
		$translation->addNote('Dodatkowa notatka');
		$translation->addDefinition('Definicja encyklopedyczna');
		$translation->addExample('Przykład użycia', 'de');

		$term->addTranslation($translation);
		$this->dict->addTerm($term);

		$xml = $this->dict->generate();
		$this->assertContains('<sense id=', $xml);
		$this->assertContains('<gloss lang=', $xml);
		$this->assertContains('<definition>', $xml);
		$this->assertContains('<note>', $xml);
		$this->assertContains('<example>', $xml);
	}

	/* ====================================================================== */
	
	public function testCanRemoveTerm()
	{
		$term = new Term('word', 'en');
		$termId = $term->getId();

		$this->dict->addTerm($term);
		$this->dict->removeTerm($termId);

		$xml = $this->dict->generate();
		$this->assertNotContains($termId, $xml);
	}
}