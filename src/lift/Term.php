<?php
/**
 * LIFT writer
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

namespace ArteQ\LIFT;

class Term
{
	/**
	 * @var string
	 */
	public $id;

	/**
	 * Term text
	 * @var string
	 */ 
	protected $text;

	/**
	 * Language code
	 * @var string
	 */ 
	protected $lang;

	/**
	 * List of term translations
	 * @var array
	 */ 
	protected $translations = [];

	/* ====================================================================== */
	
	/**
	 * Create new Term item
	 * 
	 * @param string $text
	 * @param string $lang
	 * @param string $id [optional] uuid will be generated if empty
	 */ 
	public function __construct($text, $lang, $id = null)
	{
		if (empty($id))
			$id = Util::uuid();

		$this->text = $text;
		$this->lang = $lang;
		$this->id = $id;
	}

	/* ====================================================================== */
	
	/**
	 * Return id of Term
	 * 
	 * @return string
	 */ 
	public function getId()
	{
		return $this->id;
	}

	/* ====================================================================== */
	
	/**
	 * Add new Translation to Term
	 * 
	 * @param Translation $translation
	 */ 
	public function addTranslation(Translation $translation)
	{
		$this->translations[] = $translation;
	}

	/* ====================================================================== */
	
	/**
	 * Generate DOM fragment for current Term with all Translations
	 * 
	 * @param \DOMDocument $dom
	 * @return \DOMElement
	 */ 
	public function getEntry(\DOMDocument $dom)
	{
		$entry = $dom->createElement('entry');
		$entry->setAttribute('id', $this->id);

		$lexicalUnit = $dom->createElement('lexical-unit');

		$lexicalUnitForm = $dom->createElement('form');
		$lexicalUnitForm->setAttribute('lang', $this->lang);

		$lexicalUnitFormText = $dom->createElement('text', htmlspecialchars($this->text));

		$lexicalUnitForm->appendChild($lexicalUnitFormText);
		$lexicalUnit->appendChild($lexicalUnitForm);
		$entry->appendChild($lexicalUnit);

		if (!empty($this->translations))
		{
			foreach ($this->translations as $translation)
			{
				$entry->appendChild($translation->getEntry($dom));
			}
		}

		return $entry;
	}
}