<?php
/**
 * LIFT dictionary creator
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

namespace ArteQ\LIFT;

class Dictionary
{
	const ENCODING = "UTF-8";
	const XML_VERSION = "1.0";
	const LIFT_VERSION = "0.15";

	/**
	 * List of terms
	 * @var array
	 */ 
	private $terms = [];

	/**
	 * @var \DOMDocument
	 */ 
	private $dom;

	/* ====================================================================== */
	
	/**
	 * Create new Dictionary, set XML version and encoding
	 */ 
	public function __construct()
	{
		$dom = new \DOMDocument(self::XML_VERSION, self::ENCODING);
		$dom->formatOutput = true;

		$this->dom = $dom;
	}

	/* ====================================================================== */
	
	/**
	 * Add new Term to dictionary
	 */ 
	public function addTerm(Term $term)
	{
		$this->terms[ $term->id ] = $term;
	}

	/* ====================================================================== */
	
	/**
	 * Remove Term from dictionary
	 * 
	 * @param string $id
	 */ 
	public function removeTerm($id)
	{
		unset($this->terms[ $id ]);
	}

	/* ====================================================================== */
	
	/**
	 * Create DOM document with all terms and output XML
	 * 
	 * @return string
	 */ 
	public function generate()
	{
		$lift = $this->dom->createElement('lift');
		$lift->setAttribute('version', self::LIFT_VERSION);
		$this->dom->appendChild($lift);

		foreach ($this->terms as $term)
		{
			$lift->appendChild($term->getEntry($this->dom));
		}
		
		$xml = $this->dom->saveXML();
		return $xml;	
	}

	/* ====================================================================== */
	
	/**
	 * Generate DOM document and save as file
	 * 
	 * @param string $path
	 * @return mixed
	 */ 
	public function save($path)
	{
		if (empty($path))
			throw new \Exception("Save path cannot be empty");

		if (!is_writable(dirname($path)))
			throw new \Exception("Save path: ".dirname($path)." is not writable");;

		$xml = $this->generate();
		return file_put_contents($path, $xml);
	}
}