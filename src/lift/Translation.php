<?php
/**
 * LIFT translation item
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

namespace ArteQ\LIFT;

class Translation
{
	/**
	 * @var string
	 */ 
	public $id;

	/** 
	 * Tranlsation text
	 * @var string
	 */ 
	protected $text;

	/**
	 * Language code
	 * @var string
	 */ 
	protected $lang;

	/**
	 * List of additional notes
	 * @var array
	 */ 
	protected $note = [];

	/**
	 * List of examples
	 * @var array
	 */ 
	protected $example = [];

	/**
	 * List of additinal definitions
	 * @var array
	 */ 
	protected $definition = [];

	/* ====================================================================== */
	
	/**
	 * Create new Translation item
	 * 
	 * @param string $text
	 * @param string $lang
	 * @param string $id [optional] uuid will be generated if empty
	 */ 
	public function __construct($text, $lang, $id = null)
	{
		if (empty($id))
			$id = Util::uuid();

		$this->text = $text;
		$this->lang = $lang;
		$this->id = $id;
	}

	/* ====================================================================== */
	
	/**
	 * Return id of Translation
	 * 
	 * @return string
	 */ 
	public function getId()
	{
		return $this->id;
	}

	/* ====================================================================== */
	
	/**
	 * Add new note element for Translation
	 * 
	 * @param string $text
	 * @param string $lang
	 */ 
	public function addNote($text, $lang = null)
	{
		if (empty($lang))
			$lang = $this->lang;

		$this->note[] = ['lang' => $lang, 'text' => $text];
	}

	/* ====================================================================== */
	
	/**
	 * Add new example element for Translation
	 * 
	 * @param string $text
	 * @param string $lang
	 */
	public function addExample($text, $lang = null)
	{
		if (empty($lang))
			$lang = $this->lang;

		$this->example[] = ['lang' => $lang, 'text' => $text];
	}

	/* ====================================================================== */
	
	/**
	 * Add new definition element for Translation
	 * 
	 * @param string $text
	 * @param string $lang
	 */
	public function addDefinition($text, $lang = null)
	{
		if (empty($lang))
			$lang = $this->lang;

		$this->definition[] = ['lang' => $lang, 'text' => $text];
	}

	/* ====================================================================== */
	
	/**
	 * Generate DOM fragment for current Translation with all extra elements
	 * 
	 * @param \DOMDocument $dom
	 * @return \DOMElement
	 */ 
	public function getEntry(\DOMDocument $dom)
	{
		$additionsFields = ['definition', 'note', 'example'];

		$sense = $dom->createElement('sense');
		$sense->setAttribute('id', $this->id);

		$gloss = $dom->createElement('gloss');
		$gloss->setAttribute('lang', $this->lang);

		$text = $dom->createElement('text', htmlspecialchars($this->text));
		$gloss->appendChild($text);

		$sense->appendChild($gloss);

		foreach ($additionsFields as $field)
		{
			if (!empty($this->{$field}))
			{
				foreach ($this->{$field} as $f)
				{
					$_field = $dom->createElement($field);

					$_form = $dom->createElement('form');
					$_form->setAttribute('lang', $f['lang']);

					$_text = $dom->createElement('text', htmlspecialchars($f['text']));

					$_form->appendChild($_text);
					$_field->appendChild($_form);
					$sense->appendChild($_field);
				}
			}
		}

		return $sense;
	}
}